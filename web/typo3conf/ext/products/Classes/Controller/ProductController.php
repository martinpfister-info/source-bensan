<?php
namespace Pfister\Products\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Martin Pfister <mail@martinpfister.info>, martinpfister.info
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use Pfister\Products\Domain\Model\Product;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class ProductController extends ActionController
{
    /**
     * productRepository
     *
     * @var \Pfister\Products\Domain\Repository\ProductRepository
     * @inject
     */
    protected $productRepository = null;

    /**
     * productRepository
     *
     * @var \Pfister\Products\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $categories = null;

        if (null !== $this->settings['category']) {
            $categories = $this->categoryRepository->listParentAndChildren($this->settings['category']);
        }

        if (null !== $categories) {
            $this->view->assign(
                'products',
                $this->productRepository->findByCategories($categories)
            );
        }

        $this->view->assign('detail-pid', $this->settings['detailPid']);
    }
    
    /**
     * action show
     *
     * @param \Pfister\Products\Domain\Model\Product $product
     * @return void
     */
    public function showAction(Product $product)
    {
        $this->view->assign('product', $product);
    }
}
