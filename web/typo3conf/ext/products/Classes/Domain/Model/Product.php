<?php
namespace Pfister\Products\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Martin Pfister <mail@martinpfister.info>, martinpfister.info
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Product extends AbstractEntity
{
    /**
     * title
     *
     * @var string
     */
    protected $title = '';
    
    /**
     * subtitle
     *
     * @var string
     */
    protected $subtitle = '';

    /**
     * availableRetail
     *
     * @var string
     */
    protected $availableRetail = '';

    /**
     * availableLink
     *
     * @var string
     */
    protected $availableLink = '';
    
    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * inhaltsangaben
     *
     * @var string
     */
    protected $inhaltsangaben = '';

    /**
     * serviervorschlag
     *
     * @var string
     */
    protected $serviervorschlag = '';
    
    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $image = null;
    
    /**
     * gallery
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $gallery = null;
    
    /**
     * categories
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\Category
     * @cascade remove
     */
    protected $categories = null;

    /**
     * @var bool
     */
    protected $showDetailLink = false;
    
    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    /**
     * Returns the subtitle
     *
     * @return string $subtitle
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }
    
    /**
     * Sets the subtitle
     *
     * @param string $subtitle
     * @return void
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->categories = new ObjectStorage();
    }
    
    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }
    
    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }
    
    /**
     * Returns the gallery
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $gallery
     */
    public function getGallery()
    {
        return $this->gallery;
    }
    
    /**
     * Sets the gallery
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $gallery
     * @return void
     */
    public function setGallery(\TYPO3\CMS\Extbase\Domain\Model\FileReference $gallery)
    {
        $this->gallery = $gallery;
    }
    
    /**
     * Adds a Category
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\Category $category
     * @return void
     */
    public function addCategory(\TYPO3\CMS\Extbase\Domain\Model\Category $category)
    {
        $this->categories->attach($category);
    }
    
    /**
     * Removes a Category
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\Category $categoryToRemove The Category to be removed
     * @return void
     */
    public function removeCategory(\TYPO3\CMS\Extbase\Domain\Model\Category $categoryToRemove)
    {
        $this->categories->detach($categoryToRemove);
    }
    
    /**
     * Returns the categories
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\Category $categories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    function getCategory()
    {
        return $this->getCategories();
    }
    
    /**
     * Sets the categories
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\Category $category
     * @return void
     */
    public function setCategories($category)
    {
        $this->categories = $category;
    }

    function setCategory($category)
    {
        $this->setCategories($category);
    }

    /**
     * @return string
     */
    public function getServiervorschlag(): string
    {
        return $this->serviervorschlag;
    }

    /**
     * @param string $serviervorschlag
     */
    public function setServiervorschlag(string $serviervorschlag)
    {
        $this->serviervorschlag = $serviervorschlag;
    }

    /**
     * @return string
     */
    public function getInhaltsangaben(): string
    {
        return $this->inhaltsangaben;
    }

    /**
     * @param string $inhaltsangaben
     */
    public function setInhaltsangaben(string $inhaltsangaben)
    {
        $this->inhaltsangaben = $inhaltsangaben;
    }

    /**
     * @return string
     */
    public function getAvailableRetail(): string
    {
        return $this->availableRetail;
    }

    /**
     * @param string $availableRetail
     */
    public function setAvailableRetail(string $availableRetail)
    {
        $this->availableRetail = $availableRetail;
    }
    /**
     * @return string
     */
    public function getAvailableLink(): string
    {
        return $this->availableLink;
    }

    /**
     * @param string $availableLink
     */
    public function setAvailableLink(string $availableLink)
    {
        $this->availableLink = $availableLink;
    }

    public function getShowDetailLink()
    {
        if ($this->getDescription() !== '') {
            return true;
        }
        if ($this->getAvailableRetail() !== '') {
            return true;
        }
        if ($this->getServiervorschlag() !== '') {
            return true;
        }
        if ($this->getInhaltsangaben() !== '') {
            return true;
        }

        return false;
    }
}
