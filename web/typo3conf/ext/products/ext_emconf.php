<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Bensan Product Extension',
	'description' => '',
	'category' => 'plugin',
	'author' => 'Martin Pfister',
	'author_email' => 'mail@martinpfister.info',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '1',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '1.0.0',
	'constraints' => array(
		'depends' => array(
			'typo3' => '7.6.0-7.6.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);
