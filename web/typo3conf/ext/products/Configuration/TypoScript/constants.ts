
plugin.tx_products_products {
	view {
		# cat=plugin.tx_bensanproducts_products/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:products/Resources/Private/Templates/
		# cat=plugin.tx_bensanproducts_products/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:products/Resources/Private/Partials/
		# cat=plugin.tx_bensanproducts_products/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:products/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_bensanproducts_products//a; type=string; label=Default storage PID
		storagePid =
	}
}
