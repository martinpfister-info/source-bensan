<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Pfister.' . $_EXTKEY,
	'Products',
	[
		'Product' => 'list, show',
        'Category' => 'list',

    ],
	[
		'Product' => '',
    ]
);
