<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

# read extension 'backend' configuration
$backendExtConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend']);
if (empty($backendExtConf['loginLogo'])) {
    $backendExtConf['loginLogo'] = 'EXT:' . $_EXTKEY . '/Resources/Public/Backend/Skin/img/logo_login.png';
}
if (empty($backendExtConf['loginBackgroundImage'])) {
    $backendExtConf['loginBackgroundImage'] = 'EXT:' . $_EXTKEY . '/Resources/Public/Backend/Skin/img/bg_login.jpg';
}
# Re-serialize 'backend' extension configuration
$GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend'] = serialize($backendExtConf);
