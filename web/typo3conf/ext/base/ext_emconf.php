<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'template',
    'description' => 'bensan.ch Base Extension',
    'category' => 'templates',
    'state' => 'stable',
    'clearCacheOnLoad' => 1,
    'author' => 'Martin Pfister',
    'author_email' => 'mail@martinpfister.info',
    'author_company' => 'martinpfister.info',
    'version' => '8.7.1',
    'uploadfolder' => false,
    'constraints' => [
        'depends' => [
            'php' => '7.0.0-0.0.0',
            'typo3' => '8.7.0-8.99.99',
            'fluid_styled_content' => '8.7.0-8.99.99',
            'fluid' => '8.7.0-8.99.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
