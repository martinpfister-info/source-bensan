
tt_content {
    base_teaser =< lib.fluidContent
    base_teaser {
        templateName = Teaser
        dataProcessing {
            10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
            10 {
                references.fieldName = assets
            }
        }
    }
}
