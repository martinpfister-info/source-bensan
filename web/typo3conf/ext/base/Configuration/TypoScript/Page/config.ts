config {
    doctype = html5
    xmlprologue = none
    disablePrefixComment = 1

    index_enable = 1
    index_externals = 1
    index_metatags = 1

    # css and js compression
    removeDefaultJS = 1
    inlineStyle2TempFile = 0
    compressJs = {$site.compressAndMergeAssets}
    compressCss = {$site.compressAndMergeAssets}
    concatenateJs = {$site.compressAndMergeAssets}
    concatenateCss = {$site.compressAndMergeAssets}

    # RealURL
    absRefPrefix = /
    tx_realurl_enable = {$site.enableRealURL}
    baseURL = https://www.bensan.ch

    # Spam
    spamProtectEmailAddresses = ascii

    # check for valid L-params
    linkVars = L(0-10)

    # Links & content sharing across domains
    typolinkEnableLinksAcrossDomains = 1
    typolinkCheckRootline = 1
    content_from_pid_allowOutsideDomain = 1

    # Cache
    cache_clearAtMidnight = 1
    cache_period = 1200
    sendCacheHeaders = 1

    # Title
    pageTitle.cObject = COA
    pageTitle.cObject {
        10 = TEXT
        10.value = {$site.pageTitlePrefix}
        # Insert space character between prefix and page title, if prefix is set
        10.stdWrap.noTrimWrap = || |
        10.stdWrap.if.isTrue = {$site.pageTitlePrefix}

        20 = TEXT
        20.field = subtitle // title
        20.override.cObject = TEXT
        20.override.cObject.data = TSFE:altPageTitle

        30 = TEXT
        30.value = {$site.pageTitleSuffix}
        # Insert space character between page title and suffix, if suffix is set
        30.stdWrap.noTrimWrap = | ||
        30.stdWrap.if.isTrue = {$site.pageTitleSuffix}
    }

    headerComment (
        www.martinpfister.info
    )
}
