# **********************************************************
# Base
# **********************************************************
<INCLUDE_TYPOSCRIPT: source="FILE:./Page/config_language.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Page/config.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Page/page.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Page/page_felayoutrendering.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Page/page_jscssincludes.ts">

# **********************************************************
# Extensions setups
# **********************************************************
<INCLUDE_TYPOSCRIPT: source="FILE:./../../Resources/Private/Extensions/fluid_styled_content/setup.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./../../Resources/Private/Extensions/products/setup.ts">

# **********************************************************
# Load objects to be rendered
# **********************************************************
<INCLUDE_TYPOSCRIPT: source="FILE:./Content/lib/loadContent.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Content/lib/headerImage.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Content/lib/menu.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Content/lib/submenu.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Content/lib/menu-language.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Content/lib/currentyear.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Content/lib/skiplinks.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Content/lib/pagetitle.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:./Content/lib/pagesubtitle.ts">
