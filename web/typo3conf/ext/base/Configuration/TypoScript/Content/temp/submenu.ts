#-------------------------------------------------------------------------------
#    Menu
#-------------------------------------------------------------------------------
temp.submenu = HMENU
temp.submenu {
    wrap = <nav role="navigation"><ul class="vertical menu">|</ul></nav>
    entryLevel = 1

    1 = TMENU
    1 {
        expAll = 1

        NO = 1
        NO {
            wrapItemAndSub = <li>|</li>
            ATagParams = class="layout-{field:layout}"
            allStdWrap.insertData = 1
            stdWrap.htmlSpecialChars = 1
            ATagTitle.field = subtitle // title
        }

        ACT <.NO
        ACT.wrapItemAndSub = <li class="active">|</li>
        ACT.ATagParams = class="active layout-{field:layout}"
        ACT.ATagBeforeWrap = 1

        CUR <.ACT

        CURIFSUB = 1
        CURIFSUB < .ACT
        CURIFSUB.wrapItemAndSub = <li>|</li>

        IFSUB <.NO
        IFSUB {
        wrapItemAndSub = <li>|</li>
        ATagBeforeWrap = 1
        }

        ACTIFSUB <.IFSUB
        ACTIFSUB.wrapItemAndSub = <li class="active">|</li>

        SPC <.NO
        SPC.wrapItemAndSub = <li class="divider"><label>|</label></li>
    }


    2 < .1
    2.wrap = <ul class="nested vertical menu">|</ul>

    3 < .2
}