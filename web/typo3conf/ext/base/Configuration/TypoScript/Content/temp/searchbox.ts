lib.searchbox = USER
lib.searchbox {
        userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
        vendorName = TYPO3\CMS
        extensionName = IndexedSearch
        pluginName = Pi2
        switchableControllerActions {
            Search {
                1 = form
            }
        }
    view =< plugin.tx_indexedsearch.view
    settings =< plugin.tx_indexedsearch.settings
}