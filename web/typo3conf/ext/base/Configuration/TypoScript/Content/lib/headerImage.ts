lib.headerImage = FILES
lib.headerImage {
    references.data = levelmedia:-1,slide
    maxItems = 1
    renderObj = COA
    renderObj.10 = IMG_RESOURCE
    renderObj.10.file {
        import.data = file:current:uid
        treatIdAsReference = 1
        width = 1200c
        height = 1000c
    }
}