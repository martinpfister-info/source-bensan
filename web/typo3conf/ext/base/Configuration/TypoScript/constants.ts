# **********************************************************
#    Extension constants
# **********************************************************
<INCLUDE_TYPOSCRIPT: source="FILE:./../../Resources/Private/Extensions/fluid_styled_content/constants.ts">

# **********************************************************
#    Company constants
# **********************************************************
company {
    name = Bensan AG
    street = Glütschbachstrasse 2
    pobox =
    city =  3661 Uetendorf
    phone = +41 31 318 01 60
    email = ibrahim.ben@bensan.ch
}

# **********************************************************
#    Site settings
# **********************************************************
site {

    url = http://www.bensan.local

    enableRealURL = 1

    # Page title
    pageTitlePrefix < company.name
    pageTitlePrefix := appendString( -)
    pageTitleSuffix =

    # Languages
    languageUids = 0,1
    languageLabels = DE |*| FR

    # Compress / merge CSS / JS
    compressAndMergeAssets = 0

    # Google analytics
    googleAnalytics = 0
    googleAnalytics.account = UA-XXXXXX
}
