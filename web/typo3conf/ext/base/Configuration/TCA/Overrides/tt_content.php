<?php

$languageFilePrefix = 'LLL:EXT:fluid_styled_content/Resources/Private/Language/Database.xlf:';
$frontendLanguageFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';

$contentElementName = 'base_teaser';

// Add the CType "sozialinfo_slide"
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'Teaser',
        $contentElementName,
        'content-image'
    ],
    'textmedia',
    'after'
);

//Define the palette
$GLOBALS['TCA']['tt_content']['palettes']['teaser'] = [
    'showitem' => 'header;Titel,header_link;Seite'
];

// Define what fields to display
$GLOBALS['TCA']['tt_content']['types'][$contentElementName] = [
    'showitem' => '--palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
                   --palette--;;teaser,
                   --div--;' . $frontendLanguageFilePrefix . 'tabs.media,
				    assets,',
    'columnsOverrides' => [
        'media' => [
            'label'  => $languageFilePrefix . 'tt_content.media_references',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('media', [
                'appearance'    => [
                    'createNewRelationLinkTitle' => $languageFilePrefix . 'tt_content.media_references.addFileReference'
                ],
                // custom configuration for displaying fields in the overlay/reference table
                // behaves the same as the image field.
                'foreign_types' => $GLOBALS['TCA']['tt_content']['columns']['image']['config']['foreign_types']
            ], $GLOBALS['TYPO3_CONF_VARS']['SYS']['mediafile_ext'])
        ]
    ]
];
