//root namespace
var web = web || {};

//menu modul
web.headerImage = {

    config: {
        wrapper: '#wrapper'
    },

    set: function() {
        var imgPath         = $(this.config.wrapper).attr('data-background'),
            absoluteImgPath = 'https://' + window.location.hostname + imgPath;

        $(this.config.wrapper).css('backgroundImage', 'url(' + absoluteImgPath + ')');
    },

    init: function() {
        if (Foundation.MediaQuery.atLeast('medium')) {
            this.set();
        }  else {
            $(this.config.wrapper).removeAttr("style");
        }
    }
};
