module.exports = function(grunt) {

    require( 'load-grunt-tasks' )( grunt );

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        // Adds vendor prefixes to given stylesheets.
        autoprefixer: {
            browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3'],
            options: { },
            app: { src: 'css/app.css' },
            rte: { src: '../Backend/RTE.css'}
        },

        // Compiles Sass to CSS and generates necessary files if requested
        sass: {
            options: {
                sourceMap: true,
                sourceMapEmbed: true,
                sourceMapContents: true,
                includePaths: ['bower_components/foundation-sites/scss',
                               'bower_components/foundation-sites']
            },
            dist: {
                files: {
                    'css/app.css': 'sass/app.scss'
                }
            },
            // Generate RTE css
            rte: {
                files: {
                    '../Backend/RTE.css': 'sass/RTE.scss'
                }
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'bower_components/foundation-sites/',
                        src: [
                                'js/foundation.core.js',
                                'js/foundation.util.mediaQuery.js',
                                'js/foundation.tabs.js',
                                'dist/foundation.min.js'
                        ],
                        dest: 'js/frameworks/foundation/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'bower_components/jquery/dist/',
                        src: ['**'],
                        dest:'js/frameworks/jquery'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'bower_components/lightbox2/src/',
                        src: [
                            'js/lightbox.js'
                        ],
                        dest: 'js/frameworks/lightbox/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: 'bower_components/lightbox2/src/images',
                        src: ['**'],
                        dest: 'images/',
                        filter: 'isFile'
                    }
                ]
            }
        },

        // File change watcher
        watch: {
            grunt: {
                files: ['Gruntfile.js']
            },
            sass: {
                files: 'sass/**/*.scss',
                tasks: ['buildCSS']
            }
        }

    });


    // Define tasks
    grunt.registerTask('compileSass', ['sass', 'autoprefixer']);
    grunt.registerTask('buildCSS', ['compileSass']);

    // Define default task
    grunt.registerTask('default', ['buildCSS', 'copy', 'watch']);

};
