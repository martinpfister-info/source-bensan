# Include original setup
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluid_styled_content/Configuration/TypoScript/setup.txt">

# Override/extend setup
lib.contentElement {
    templateRootPaths.10 = EXT:base/Resources/Private/Extensions/fluid_styled_content/Templates/
    layoutRootPaths.10 = EXT:base/Resources/Private/Extensions/fluid_styled_content/Layouts/
    partialRootPaths.10 = EXT:base/Resources/Private/Extensions/fluid_styled_content/Partials/
}