# Include original setup
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:products/Configuration/TypoScript/setup.ts">


plugin.tx_products_products {
    view {
        templateRootPaths {
            10 = EXT:base/Resources/Private/Extensions/products/Templates
        }
        partialRootPaths{
            10 = EXT:base/Resources/Private/Extensions/products/Partials
        }
        layoutRootPaths {
            10 = EXT:base/Resources/Private/Extensions/products/Layouts
        }
    }
}
