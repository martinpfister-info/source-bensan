#-------------------------------------------------------------------------------
#  Pages
#-------------------------------------------------------------------------------
TCEFORM.pages {

    # Page alias
    alias.disabled = 1

    # Abstract
    abstract.disabled = 1

    # Keywords meta tag
    keywords.disabled = 1

    # Categories
    categories.disabled = 0

    # Page type "backend user section" (remove)
    doktype.removeItems = 6

    # Frontend layouts
    # (disabled per default)
    layout {
        disabled = 1

        # Purge all items, only keep "default / inherit" (displayed as empty option field)
        #keepItems = 0
        #addItems.1 = LLL:EXT:sozialinfo/Resources/Private/Language/Backend.xlf:frontend_layout.examplelayout
        #addItems.1.icon = EXT:sozialinfo/path/to/your/icon.gif
    }

    # Backend layouts
    # Remove "none"
    backend_layout.removeItems = -1
    backend_layout_next_level.removeItems = -1

    # "new until"
    newUntil.disabled = 1

    # Author
    author.disabled = 1
    author_email.disabled = 1

    # Last updated
    lastUpdated.disabled = 1
}


#-------------------------------------------------------------------------------
#  Content elements
#-------------------------------------------------------------------------------
TCEFORM.tt_content {

    # Date
    date.disabled = 1

    # Categories
    categories.disabled = 0

    # Only allow headers 2, 3, and 4
    header_layout {
        keepItems = 1,2,3,100
        altLabels {
            1 = LLL:EXT:base/Resources/Private/Language/Backend.xlf:tt_content.header_layout.1
            2 = LLL:EXT:base/Resources/Private/Language/Backend.xlf:tt_content.header_layout.2
            3 = LLL:EXT:base/Resources/Private/Language/Backend.xlf:tt_content.header_layout.3
            4 = LLL:EXT:base/Resources/Private/Language/Backend.xlf:tt_content.header_layout.4
            100 = LLL:EXT:base/Resources/Private/Language/Backend.xlf:tt_content.header_layout.100
        }
    }

    # Content element layouts
    layout.disabled = 0
    layout.keepItems = 0,1
    layout.altLabels.1 = LLL:EXT:sozialinfo/Resources/Private/Language/Backend.xlf:tt_content.layout.examplelayout

    # Image border
    imageborder.disabled = 1

    # Image positioning
    # 0 - Above, center
    # 1 - Above, right
    # 2 - Above, left
    # 8 - Below, center
    # 9 - Below, right
    # 10 - Below, left
    # 17 - In text, right
    # 18 - in text, left
    imageorient.removeItems = 0,8,9
}

#-------------------------------------------------------------------------------
#  Default language
#-------------------------------------------------------------------------------
mod.SHARED {
    defaultLanguageLabel = Deutsch
    defaultLanguageFlag = de
}

#-------------------------------------------------------------------------------
# Defaults for new content elements
#-------------------------------------------------------------------------------
TCAdefaults.tt_content {
    header_layout = 1
    imagecols = 1
    imageorient = 2
}

#-------------------------------------------------------------------------------
#  Permissions for new pages
#-------------------------------------------------------------------------------
TCEMAIN.permissions {
    # Owner
    userid = 1
    # Group
    groupid = 1
    # Permissions
    user = show, editcontent, edit, new, delete
    group = show, editcontent, edit, new, delete
}


#-------------------------------------------------------------------------------
#  Control elements
#-------------------------------------------------------------------------------
mod {
    # Always show "extended view"
    #file_list.enableDisplayBigControlPanel = activated
    #web_list.enableDisplayBigControlPanel = activated

    # Hide "Quick Edit" & "Page Information" in dropdown on the very top of a page
    #web_layout.menu.function {
    #    0 = 0
    #    3 = 0
    #}
}